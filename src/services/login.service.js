import apiUrl from "./utils/apiUrl";
import Api from "./utils/Api";
import httpStatus from "./utils/httpstatus";
import validateResponse from "./utils/valiadateResponse";
import httpHelper from "./utils/httpHelper";

export default {
  /**
   *
   * @param {Object} payload
   * @param {String} payload.cell_phone numero de telefono
   * @param {String} payload.password contraseña de usuario
   * @param {String} payload.device_name device tomado del navegador
   * @returns Token
   */
  async login(payload) {
    try {
      const url = apiUrl.LOGIN;
      const response = await Api.post(url, payload);
      return validateResponse.formatResponse(response);
    } catch (error) {
      return Promise.reject(new Error("Error"));
    }
  },

  /**
   * obtiene la informacion del usuario logueado
   * @returns
   */
  async user() {
    try {
      const url = apiUrl.USER_AUTH;
      const response = await Api.get(url, httpHelper.getHeaders());
      return validateResponse.formatResponse(response);
    } catch (error) {
      return Promise.reject(new Error("Error"));
    }
  },
};
