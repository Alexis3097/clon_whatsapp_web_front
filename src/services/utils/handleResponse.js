import { statusOptions } from "./handleStatus";

export const handleSuccess = (response) => response;

export function handleError(error) {
  const status = error.response.status;
  return statusOptions[status]
    ? statusOptions[status](error)
    : statusOptions["default"](error);
}
