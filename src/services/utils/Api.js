import axios from "axios";
import { handleSuccess, handleError } from "./handleResponse";
const Api = axios.create({
  baseURL: `${process.env.APP_API}`,
});
Api.interceptors.response.use(handleSuccess, handleError);

export default Api;
