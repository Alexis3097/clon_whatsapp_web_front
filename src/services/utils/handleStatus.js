const baseUrl = process.env.APP_SERVER;
import router from "../../router/index";
/**
 * Acciones del manejador de errores
 */
export const actions = {
  /**
   * Reedirige hacia la vista del login
   */
  redirectToLogoutMix: (error) => {
    console.log(router);
    router.replace({ path: "/", replace: true });
    // window.location.href = baseUrl + "mix_logout";
  },
  /**
   * Reedige a la vista de error
   */
  redirectToErrorPage: (error) => {
    // window.location.href = baseUrl + "error_500";
    console.log("pagina no encontrada");
  },

  pageNotFound: (error) => {
    console.log("pagina no encontrada");
  },
  UNPROCESSABLE_CONTENT: (error) => {
    return error.response;
  },

  defaultError: (error) => {
    return Promise.reject(error);
  },
};

/**
 * Objeto que contiene los estatus a menejar
 * y un estatus por defecto
 */
export const statusOptions = {
  // error cuando el usuario está inaturizado
  401: actions.redirectToLogoutMix,
  // error cuando ocurrió un errro en el servidor
  500: actions.redirectToErrorPage,

  422: actions.UNPROCESSABLE_CONTENT,
  // error que no cumpla con algun estatus previsto
  default: actions.defaultError,
};
