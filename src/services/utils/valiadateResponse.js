import httpStatus from "../utils/httpstatus";
export default {
  /**
   * valida la entarda de los datos
   * @param {Object} response
   */
  formatResponse(responseApi) {
    let response = null;
    try {
      return (response = {
        status: responseApi.status,
        data: responseApi.data,
      });
    } catch (error) {
      return Promise.reject(new Error(error));
    }
  },
};
