import { LocalStorage } from "quasar";
export default {
  getHeaders() {
    let token = LocalStorage.getItem("token");
    let headers = {
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
      },
    };
    return headers;
  },
};
