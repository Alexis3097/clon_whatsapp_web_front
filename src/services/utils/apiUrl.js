export default {
  LOGIN: "sanctum/token",
  GET_CONTACTS: "contacts",
  USER_AUTH: "users/auth",
  GET_CHAT_ID: "chats",
  GET_MESSAGE_X_CHAT_ID: "chats/",
  GET_CHAT_LIST: "chats/",
  CHECKED_MESSAGE: "message/",
};
