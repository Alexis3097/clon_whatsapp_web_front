import Api from "../utils/Api";
import Httphelper from "../utils/httpHelper";

/**
 * Realiza una petición de tipo get a la url indicada
 * @param {String} url url
 * @author Cristian Alexis Montoya Arguello
 * @returns Object
 */
async function get(url) {
  let response = null;
  try {
    let response = await Api.get(url, Httphelper.getHeaders());
    return response.data;
  } catch (error) {
    response = { ...error.response.data, success: false };
  }
  return response;
}

/**
 * Realiza una petición de tipo post a la url indicando su carga util
 * @param {String} url url a enviar
 * @param {any} payload Carga Util
 * @author Cristian Alexis Montoya Arguello
 * @returns Object
 */
async function post(url, payload) {
  let response = null;
  try {
    let response = await Api.post(url, payload, Httphelper.getHeaders());
    return response;
  } catch (error) {
    response = { ...error.response.data, success: false };
  }
  return response;
}

/**
 * Realiza una petición de tipo put a la url indicando su carga util
 * @param {String} url url a enviar
 * @param {any} payload Carga Util
 * @author Cristian Alexis Montoya Arguello
 * @returns
 */
async function put(url, payload) {
  let response = null;
  try {
    let response = await Api.put(url, payload, Httphelper.getHeaders());
    return response;
  } catch (error) {
    response = { ...error.response.data, success: false };
  }
  return response;
}

/**
 * Realiza una petición de tipo delete a la url indicando su carga util
 * @param {String} url url a enviar
 * @param {any} payload Carga Util
 * @author Cristian Alexis Montoya Arguello
 * @returns
 */
async function Delete(url) {
  let response = null;
  try {
    let response = await Api.delete(url, Httphelper.getHeaders());
    return response;
  } catch (error) {
    response = { ...error.response.data, success: false };
  }
  return response;
}

export default {
  get,
  post,
  put,
  Delete,
};
