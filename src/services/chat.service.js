import apiUrl from "./utils/apiUrl";
import IMContext from "./utils/IMContext";
import validateResponse from "./utils/valiadateResponse";

export default {
  async getChatList() {
    try {
      const url = apiUrl.GET_CHAT_LIST;
      const response = await IMContext.get(url);

      return validateResponse.formatResponse(response);
    } catch (error) {
      return Promise.reject(new Error("Error"));
    }
  },
};
