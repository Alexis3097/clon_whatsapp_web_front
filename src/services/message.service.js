import apiUrl from "./utils/apiUrl";
import IMContext from "./utils/IMContext";
import validateResponse from "./utils/valiadateResponse";

export default {
  /**
   *
   * @param {Object} payload
   * @param {Integer} payload.sender_id id ed mi usario
   * @param {Integer} payload.recipient_id id usuario destinatario
   * @returns Token
   */
  async getChat(payload) {
    try {
      const url = apiUrl.GET_CHAT_ID;
      const response = await IMContext.post(url, payload);
      return validateResponse.formatResponse(response);
    } catch (error) {
      return Promise.reject(new Error("Error"));
    }
  },

  /**
   *
   * @param {Integer} chat_id
   * @returns Token
   */
  async getMessagesXChatId(chat_id) {
    try {
      const url = `${apiUrl.GET_MESSAGE_X_CHAT_ID}${chat_id}/messages`;
      const response = await IMContext.get(url);
      return validateResponse.formatResponse(response);
    } catch (error) {
      return Promise.reject(new Error("Error"));
    }
  },

  /**
   *@param {Integer} chat_id
   * @param {*} payload
   * @param {string} payload.message mensaje
   * @param {Integer} payload.sender_id id usuario que manda el mensaje
   * @returns
   */
  async saveMessage(chat_id, payload) {
    try {
      const url = `${apiUrl.GET_MESSAGE_X_CHAT_ID}${chat_id}/messages`;

      const response = await IMContext.post(url, payload);
      return validateResponse.formatResponse(response);
    } catch (error) {
      return Promise.reject(new Error("Error"));
    }
  },

  /**
   *
   * @param {Integer} chatId id del chat
   * @returns
   */
  async checkedMessage(chatId) {
    try {
      const url = `${apiUrl.CHECKED_MESSAGE}${chatId}`;
      const response = await IMContext.post(url);
      return validateResponse.formatResponse(response);
    } catch (error) {}
  },
};
