import apiUrl from "./utils/apiUrl";
import Api from "./utils/Api";
import httpStatus from "./utils/httpstatus";
import validateResponse from "./utils/valiadateResponse";
import IMContext from "./utils/IMContext";
export default {
  /**
   * Obtiene la lista de contactos ordenados alfabeticamente y paginado de 10 en 10
   * @returns contactos
   */
  async getContacts() {
    try {
      const url = apiUrl.GET_CONTACTS;
      const response = await IMContext.get(url);
      return response;
    } catch (error) {
      return Promise.reject(new Error("Error"));
    }
  },
};
