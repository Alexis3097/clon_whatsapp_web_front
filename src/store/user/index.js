import types from "./mutation-types";
// import { router } from "vue-router";
const state = () => ({
  name: "",
  profile_picture: null,
  openChat: false,
  openContact: false,
  getMessage: false,
});
const getters = {
  name: (state) => state.name,
  profile_picture: (state) => state.profile_picture,
  openChat: (state) => state.openChat,
  openContact: (state) => state.openContact,
  getMessage: (state) => state.getMessage,
};
const actions = {
  async setName({ commit }, { name, profile_picture }) {
    try {
      commit(types.SET_NAME, name);
      commit(types.SET_PROFILE_PICTURE, profile_picture);
    } catch (error) {
      return Promise.reject(new Error(error));
    }
  },
  openChat({ commit }, openChat) {
    try {
      commit(types.SET_OPEN_CHAT, openChat);
    } catch (error) {}
  },

  openContact({ commit }, openContact) {
    try {
      commit(types.SET_OPEN_CONTACT, openContact);
    } catch (error) {}
  },

  changeFlat({ commit }) {
    try {
      commit(types.GET_MESSAGE);
    } catch (error) {}
  },
};

const mutations = {
  [types.SET_NAME](state, payload) {
    state.name = payload;
  },

  [types.SET_PROFILE_PICTURE](state, payload) {
    state.profile_picture = payload;
  },
  [types.SET_OPEN_CHAT](state, payload) {
    state.openChat = payload;
  },
  [types.SET_OPEN_CONTACT](state, payload) {
    state.openContact = payload;
  },
  [types.GET_MESSAGE](state) {
    state.getMessage = !state.getMessage;
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
