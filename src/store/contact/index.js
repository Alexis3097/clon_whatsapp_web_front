import types from "./mutation-types";
import contactService from "../../services/contact.service";
import { LocalStorage } from "quasar";
// import { router } from "vue-router";
const state = () => ({
  errorMessage: "",
  contacts: [],
});
const getters = {
  errorMessage: (state) => state.errorMessage,
  contacts: (state) => state.contacts,
};
const actions = {
  /**
   * Obtiene la lista de contactos ordenados alfabeticamente y paginado de 10 en 10
   * @returns contactos
   */
  async getContacts({ commit }) {
    try {
      const response = await contactService.getContacts();
      commit(types.SET_CONTACTS, response.data.data);
    } catch (error) {
      return Promise.reject(new Error("Error al obtener los contactos"));
    }
  },
};

const mutations = {
  [types.SET_CONTACTS](state, payload) {
    let user_id = LocalStorage.getItem("user_id");
    const nuevaLista = payload.filter((item) => item.id !== user_id);
    state.contacts = nuevaLista;
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
