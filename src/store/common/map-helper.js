import mapfn from "./map-fn-types";
import mapmod from "./map-module-types";
import mapget from "./map-getter-types";

export default {
  mapfn,
  mapmod,
  mapget,
};
