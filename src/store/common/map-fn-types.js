export default {
  //COIN
  LOGIN: "login",
  GET_CONTACTS: "getContacts",
  GET_CHAT: "getChat",
  GET_MESSAGES: "getMessagesXChatId",
  SEND_MESSAGE: "saveMessage",
  SET_USER: "setName",
  OPEN_CHAT: "openChat",
  OPEN_CONTACT: "openContact",
  GET_CHAT_LIST: "getChatList",
  CHECKED_MESSAGE: "checkedMessage",
  CHANGE_FLAG: "changeFlat",
};
