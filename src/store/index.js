import { store } from "quasar/wrappers";
import { createStore } from "vuex";

import login from "./login/index";
import contact from "./contact/index";
import chat from "./chat/index";
import user from "./user/index";

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      login,
      contact,
      chat,
      user,
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  });

  return Store;
});
