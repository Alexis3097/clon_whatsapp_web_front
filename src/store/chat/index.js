import types from "./mutation-types";
import messageService from "../../services/message.service";
import chatService from "../../services/chat.service";
import httpstatus from "src/services/utils/httpstatus";
import { HttpStatusCode } from "axios";
// import { router } from "vue-router";
const state = () => ({
  chat_id: -1,
  messages: [],
  chatList: [],
});
const getters = {
  chat_id: (state) => state.chat_id,
  messages: (state) => state.messages,
  chatList: (state) => state.chatList,
};
const actions = {
  /**
   * Obtiene un chat_id
   * @returns
   */
  async getChat({ commit }, payload) {
    try {
      const response = await messageService.getChat(payload);
      commit(types.CHAT_ID, response.data.data.chat_id);
    } catch (error) {
      return Promise.reject(new Error("Error al obtener los contactos"));
    }
  },

  /**
   * obtiene mensajes de un chat
   * @param {Integer} chat_id id del chat que se desea buscar sus mensajes
   * @author Cristian Alexis Montoya Arguello
   * @returns
   */
  async getMessagesXChatId({ commit }, chat_id) {
    try {
      const response = await messageService.getMessagesXChatId(chat_id);
      commit(types.SET_MESSAGES, response.data);
    } catch (error) {
      return Promise.reject(new Error("Error al obtener los contactos"));
    }
  },

  /**
   *
   * @param {*} payload
   * @param {Integer} payload.chat_id chat_id al que mandara el mensaje
   * @param {string} payload.message mensaje
   * @param {Integer} payload.sender_id id usuario que manda el mensaje
   * @returns
   */
  async saveMessage({ commit }, { chat_id, message, sender_id }) {
    try {
      const response = await messageService.saveMessage(chat_id, {
        message: message,
        sender_id: sender_id,
      });
      commit(types.ADD_MESSAJE, response.data.data);
    } catch (error) {
      return Promise.reject(new Error("Error al enviar el mensaje"));
    }
  },

  async getChatList({ commit }) {
    try {
      let response = await chatService.getChatList();
      commit(types.SET_CHAT_LIST, response.data);
    } catch (error) {}
  },

  /**
   * pasa el estatus del  mensaje a visto
   * @param {Integer} chatId
   */
  async checkedMessage({ commit }, chatId) {
    try {
      let response = await messageService.checkedMessage(chatId);
    } catch (error) {}
  },
  validarStatus(status) {
    if (status == HttpStatusCode.Unauthorized) {
      this.$router.push({ path: "/", replace: true });
    }
  },
};

const mutations = {
  [types.CHAT_ID](state, payload) {
    state.chat_id = payload;
  },
  [types.SET_MESSAGES](state, payload) {
    state.messages = payload;
  },
  [types.ADD_MESSAJE](state, payload) {
    state.messages.push(payload);
  },
  [types.SET_CHAT_LIST](state, payload) {
    state.chatList = payload;
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
