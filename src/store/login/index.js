import types from "./mutation-types";
import loginService from "../../services/login.service";
import { LocalStorage } from "quasar";
import httpStatus from "../../services/utils/httpstatus";
// import { router } from "vue-router";
const state = () => ({
  errorMessage: "",
  statusCode: 200,
});
const getters = {
  errorMessage: (state) => state.errorMessage,
  statusCode: (state) => state.statusCode,
};
const actions = {
  /**
   *
   * @param {Object} payload
   * @param {String} payload.cell_phone numero de telefono
   * @param {String} payload.password contraseña de usuario
   * @param {String} payload.device_name device tomado del navegador
   * @returns Token
   */
  async login({ commit, dispatch }, payload) {
    try {
      const response = await loginService.login(payload);
      if (response.status == httpStatus.SUCCESS) {
        dispatch(types.SET_TOKEN, response.data.token);
        dispatch(types.USER);
        commit(types.SET_STATUS_CODE, response.status);
      } else {
        commit(types.SET_ERROR_MESSAGE, response.data.message);
        commit(types.SET_STATUS_CODE, response.status);
      }
    } catch (error) {
      return Promise.reject(error);
    }
  },
  async user({ commit }) {
    try {
      const response = await loginService.user();
      LocalStorage.set("phone", response.data.data.cell_phone);
      LocalStorage.set("user_id", response.data.data.id);
      LocalStorage.set("name", response.data.data.name);
      this.$router.push({ path: "/home" });
    } catch (error) {
      console.log(error);
    }
  },
  setToken({}, token) {
    try {
      const onlyToken = token.split("|");
      LocalStorage.set("token", onlyToken[1]);
    } catch (error) {
      console.log(error);
    }
  },
};

const mutations = {
  [types.SET_ERROR_MESSAGE](state, payload) {
    state.errorMessage = payload;
  },
  [types.SET_STATUS_CODE](state, payload) {
    state.statusCode = payload;
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
