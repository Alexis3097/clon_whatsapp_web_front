export default {
  SET_ERROR_MESSAGE: "SET_ERROR_MESSAGE",
  SET_STATUS_CODE: "SET_STATUS_CODE",
  SET_TOKEN: "setToken",
  USER: "user",
};
