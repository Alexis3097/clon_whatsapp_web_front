

Este proyecto está desarrollado con Quasar Framework y se integra con una API en Laravel.

## Configuración inicial

Antes de ejecutar la aplicación Quasar, asegúrate de seguir estos pasos de configuración inicial:

1. Clona el repositorio o descarga el código fuente del proyecto.

2. Asegúrate de tener Node.js y npm instalados en tu sistema. Puedes descargarlos desde https://nodejs.org si aún no los tienes.

3. Abre una terminal en la raíz del proyecto y ejecuta el siguiente comando para instalar las dependencias:

   ```bash
   npm install
   ```

   Esto descargará todas las dependencias necesarias para la aplicación Quasar.

4. Configura el archivo `.env` en la raíz del proyecto. Puedes copiar el contenido del archivo `.env.example` y ajustarlo según tu entorno. Aquí puedes establecer la URL de la API en Laravel.


   Asegúrate de establecer correctamente la URL de la API en la que se encuentra tu aplicación Laravel.

## Uso de las credenciales por defecto

Por defecto, la aplicación Quasar utiliza las siguientes credenciales de inicio de sesión:

- telefono: `9611234567`
- password: `password`


## Antes de ejecutar el proyecto

Antes de ejecutar el proyecto Quasar, asegúrate de que la API en Laravel esté en funcionamiento. La API proporcionará los datos y servicios necesarios para el correcto funcionamiento de la aplicación Quasar.

Asegúrate de seguir las instrucciones de configuración y ejecución de la API en Laravel antes de ejecutar la aplicación Quasar.

## Ejecutar el proyecto

Una vez que hayas realizado la configuración inicial y la API en Laravel esté en ejecución, puedes iniciar la aplicación Quasar con el siguiente comando:

```bash
quasar dev
```

Esto iniciará la aplicación en el entorno de desarrollo y podrás acceder a ella a través de la URL proporcionada en la terminal.
